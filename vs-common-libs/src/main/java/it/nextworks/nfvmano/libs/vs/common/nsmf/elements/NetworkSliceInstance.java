package it.nextworks.nfvmano.libs.vs.common.nsmf.elements;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class NetworkSliceInstance {

    private UUID networkSliceInstanceId;
    private List<UUID> networkSliceSubnetIds = new ArrayList<>();
    private String vsInstanceId;
    private NetworkSliceInstanceStatus status;
    private String internalStatus;


    public NetworkSliceInstance(UUID networkSliceInstanceId, List<UUID> networkSliceSubnetIds, String vsInstanceId, NetworkSliceInstanceStatus status, String internalStatus) {
        this.networkSliceInstanceId = networkSliceInstanceId;
        this.networkSliceSubnetIds = networkSliceSubnetIds;
        this.vsInstanceId = vsInstanceId;
        this.status = status;
        this.internalStatus= internalStatus;
    }

    public UUID getNetworkSliceInstanceId() {
        return networkSliceInstanceId;
    }

    public List<UUID> getNetworkSliceSubnetIds() {
        return networkSliceSubnetIds;
    }

    public String getVsInstanceId() {
        return vsInstanceId;
    }

    public NetworkSliceInstanceStatus getStatus() {
        return status;
    }
}
