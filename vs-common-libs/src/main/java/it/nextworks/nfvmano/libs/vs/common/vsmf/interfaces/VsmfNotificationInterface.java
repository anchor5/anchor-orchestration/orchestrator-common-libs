package it.nextworks.nfvmano.libs.vs.common.vsmf.interfaces;

import it.nextworks.nfvmano.libs.vs.common.vsmf.message.VsmfNotificationMessage;

public interface VsmfNotificationInterface {

    public void notifyVsmf(VsmfNotificationMessage message);
}
