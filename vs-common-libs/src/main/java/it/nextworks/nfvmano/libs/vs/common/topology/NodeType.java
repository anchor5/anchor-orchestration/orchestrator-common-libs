package it.nextworks.nfvmano.libs.vs.common.topology;

public enum NodeType {
    COMPUTE,
    SWITCH,
    PNF
}
