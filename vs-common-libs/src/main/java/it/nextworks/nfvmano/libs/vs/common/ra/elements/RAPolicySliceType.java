package it.nextworks.nfvmano.libs.vs.common.ra.elements;

public enum RAPolicySliceType
{
    EMBB,
    URLLC,
    MMTC
}
