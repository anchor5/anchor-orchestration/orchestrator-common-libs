package it.nextworks.nfvmano.libs.vs.common.topology;

public enum ProcessingCapabilities {
    NONE,
    MEC,
    CLOUD,
    FOG,
    EDGE
}
