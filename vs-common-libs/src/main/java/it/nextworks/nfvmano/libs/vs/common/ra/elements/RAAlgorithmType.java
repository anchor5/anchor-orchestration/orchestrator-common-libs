package it.nextworks.nfvmano.libs.vs.common.ra.elements;

public enum RAAlgorithmType {
    STATIC,
    FILE,
    EXTERNAL
}
