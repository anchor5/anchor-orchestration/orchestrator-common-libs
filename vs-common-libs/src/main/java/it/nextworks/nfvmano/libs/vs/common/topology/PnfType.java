package it.nextworks.nfvmano.libs.vs.common.topology;

public enum PnfType {
    BS,
    SC,
    gNB
}
