package it.nextworks.nfvmano.libs.vs.common.ra.elements;

public enum TransportFlowType {

    SATELLITE,
    TERRESTRIAL
}
