package it.nextworks.nfvmano.libs.vs.common.nsmf.messages.configuration;

public enum ConfigSliceSubnetType {

    CORE,
    TRANSPORT,
    RAN,
    APP
}
