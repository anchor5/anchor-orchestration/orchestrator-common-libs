package it.nextworks.nfvmano.libs.vs.common.vsmf.elements;

public enum NsiNotifType {
    STATUS_CHANGED,
    ERROR
}
