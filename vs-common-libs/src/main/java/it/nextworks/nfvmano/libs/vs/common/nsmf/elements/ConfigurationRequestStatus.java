package it.nextworks.nfvmano.libs.vs.common.nsmf.elements;

public enum ConfigurationRequestStatus {
    IN_PROGRESS,
    SUCCESS,
    FAILED
}
