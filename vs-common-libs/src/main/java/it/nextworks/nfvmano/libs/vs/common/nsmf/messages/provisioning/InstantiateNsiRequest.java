package it.nextworks.nfvmano.libs.vs.common.nsmf.messages.provisioning;

import it.nextworks.nfvmano.libs.vs.common.nsmf.messages.NsmfGenericNsiMessage;

import java.util.UUID;

public class InstantiateNsiRequest extends NsmfGenericNsiMessage {



    public InstantiateNsiRequest(){};
    public InstantiateNsiRequest(UUID nsiId){
        super(nsiId);
    }
}
