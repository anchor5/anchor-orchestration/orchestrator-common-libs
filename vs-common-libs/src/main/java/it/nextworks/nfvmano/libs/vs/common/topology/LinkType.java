package it.nextworks.nfvmano.libs.vs.common.topology;

public enum LinkType {
    WIRED,
    WIRELESS
}
