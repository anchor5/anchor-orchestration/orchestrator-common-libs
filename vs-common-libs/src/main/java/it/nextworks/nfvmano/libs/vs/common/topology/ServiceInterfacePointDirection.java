package it.nextworks.nfvmano.libs.vs.common.topology;

public enum ServiceInterfacePointDirection {
    BIDERECTIONAL,
    INPUT,
    OUTPUT
}
