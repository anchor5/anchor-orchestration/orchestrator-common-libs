package it.nextworks.nfvmano.libs.vs.common.nsmf.messages.configuration;

public enum ConfigurationActionType {
    SERVICE_FLOW_TRANSFER,
    SLICE_TRANSFER,
    SATELLITE_NETWORK_CONFIGURATION
}
