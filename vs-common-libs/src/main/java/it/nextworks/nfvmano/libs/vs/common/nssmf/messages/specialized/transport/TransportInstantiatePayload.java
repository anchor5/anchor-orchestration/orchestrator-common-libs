package it.nextworks.nfvmano.libs.vs.common.nssmf.messages.specialized.transport;

import com.fasterxml.jackson.annotation.JsonProperty;
import it.nextworks.nfvmano.libs.vs.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.vs.common.nssmf.messages.provisioning.NssmfBaseProvisioningMessage;
import it.nextworks.nfvmano.libs.vs.common.ra.elements.NssResourceAllocation;

import java.util.UUID;

public class TransportInstantiatePayload extends NssmfBaseProvisioningMessage {

    @JsonProperty("payload")
    private NssResourceAllocation nssResourceAllocation;

    public TransportInstantiatePayload() {}

    public TransportInstantiatePayload(UUID nssiId, NssResourceAllocation nssResourceAllocation) {
        super(nssiId);
        this.nssResourceAllocation = nssResourceAllocation;
    }

    public void isValid()throws MalformattedElementException{
        if(nssResourceAllocation==null)
            throw new MalformattedElementException("NssResourceAllocation object cannot be null");
    }

    public NssResourceAllocation getNssResourceAllocation() {
        return nssResourceAllocation;
    }

    public void setNssResourceAllocation(NssResourceAllocation nssResourceAllocation) {
        this.nssResourceAllocation = nssResourceAllocation;
    }
}
