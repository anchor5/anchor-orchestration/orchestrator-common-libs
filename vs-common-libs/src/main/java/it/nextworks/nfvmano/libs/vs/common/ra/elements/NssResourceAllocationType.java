package it.nextworks.nfvmano.libs.vs.common.ra.elements;

public enum NssResourceAllocationType {
    COMPUTE,
    TRANSPORT,
    SDN
}
