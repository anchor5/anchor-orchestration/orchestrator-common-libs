/*
 * Copyright 2021 Nextworks s.r.l.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.nfvmano.catalogues.nstnesttranslation;

import it.nextworks.nfvmano.catalogues.nstnesttranslation.messages.TranslationRequest;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.templates.gst.GST;

public interface NstNestTranslationInterface {

    /**
     * This method provides a GSMA NEST as a result of the translation
     * of a provided 3GPP NST
     * @param request translation request
     * @return nest obtained from translation
     * @throws FailedOperationException
     */
    public GST translateNst(TranslationRequest request) throws FailedOperationException, MalformattedElementException;
}
