package it.nextworks.nfvmano.catalogues.nstnesttranslation.messages;

import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.templates.gst.GST;
import it.nextworks.nfvmano.libs.ifa.templates.nst.NST;

public class TranslationRequest {

    private String nstId;
    private NST nst;
    private String nestId;
    private GST nest;

    public TranslationRequest(){}

    public TranslationRequest(NST nst, String nestId, GST nest){
        this.nst=nst;
        this.nestId=nestId;
        this.nest=nest;
    }

    public void isValid() throws MalformattedElementException {
        if(nst==null && nstId==null)
            throw new MalformattedElementException("NST and NstId cannot be both null");
        if(nst!=null && nstId!=null)
            throw new MalformattedElementException("NST and NstId cannot be both not null");
        if(nestId==null && nest==null){
            if(nst!=null)
                nestId="nest-"+nst.getNstId();
            else
                nestId="nest-"+nstId;
        }
    }

    public String getNstId() {
        return nstId;
    }

    public void setNstId(String nstId) {
        this.nstId = nstId;
    }

    public NST getNst() {
        return nst;
    }

    public void setNst(NST nst) {
        this.nst = nst;
    }

    public String getNestId() {
        return nestId;
    }

    public void setNestId(String nestId) {
        this.nestId = nestId;
    }

    public GST getNest() {
        return nest;
    }

    public void setNest(GST nest) {
        this.nest = nest;
    }
}
