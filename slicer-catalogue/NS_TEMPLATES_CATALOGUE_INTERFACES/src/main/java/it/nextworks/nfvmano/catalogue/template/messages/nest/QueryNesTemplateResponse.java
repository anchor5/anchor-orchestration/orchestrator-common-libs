package it.nextworks.nfvmano.catalogue.template.messages.nest;

import it.nextworks.nfvmano.catalogue.template.elements.GsTemplateInfo;
import it.nextworks.nfvmano.catalogue.template.elements.NsTemplateInfo;
import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

import java.util.ArrayList;
import java.util.List;

public class QueryNesTemplateResponse implements InterfaceMessage {
    private List<GsTemplateInfo> nesTemplateInfos = new ArrayList<>();

    public QueryNesTemplateResponse(){}

    /**
     * Constructor
     * @param nsTemplateInfos List of NSTemplate info
     */
    public QueryNesTemplateResponse(List<GsTemplateInfo> nsTemplateInfos) {
        this.nesTemplateInfos = nsTemplateInfos;
    }

    public void setNsTemplateInfos(List<GsTemplateInfo> nsTemplateInfos) {
        this.nesTemplateInfos = nsTemplateInfos;
    }

    /**
     * @return the nsTemplateInfo
     */
    public List<GsTemplateInfo> getGsTemplateInfos() {
        return nesTemplateInfos;
    }

    @Override
    public void isValid() throws MalformattedElementException {
        if(nesTemplateInfos!=null)
            for (GsTemplateInfo nestInfo : nesTemplateInfos) nestInfo.isValid();
        else
            throw new MalformattedElementException("NsTemplateInfos value is required");
    }
}
