package it.nextworks.nfvmano.catalogue.template.messages.nest;

import it.nextworks.nfvmano.catalogue.template.elements.NstConfigurationRule;
import it.nextworks.nfvmano.catalogue.template.messages.nst.OnBoardNsTemplateRequest;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.OnBoardVnfPackageRequest;
import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.descriptors.nsd.Nsd;
import it.nextworks.nfvmano.libs.ifa.descriptors.nsd.Pnfd;
import it.nextworks.nfvmano.libs.ifa.templates.gst.GST;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class OnBoardNesTemplateRequest implements InterfaceMessage {

    private static final Logger log = LoggerFactory.getLogger(OnBoardNsTemplateRequest.class);

    private GST gst;

    private String nstId;
    private List<Nsd> nsds = new ArrayList<>();
    private List<OnBoardVnfPackageRequest> vnfPackages = new ArrayList<>();
    private List<Pnfd> pnfds = new ArrayList<>();

    private List<NstConfigurationRule> configurationRules = new ArrayList<>();

    public OnBoardNesTemplateRequest() { }

    /**
     * Constructor
     * @param gst the generic network slice template
     * @param nsds a list of network slice descriptor
     * @param vnfPackages a list of VNF packages
     */
    public OnBoardNesTemplateRequest(GST gst, String nstId, List<Nsd> nsds, List<OnBoardVnfPackageRequest> vnfPackages){
        this.gst=gst;
        this.nstId=nstId;
        if(this.nsds!=null)
            this.nsds = nsds;
        if(vnfPackages!=null)
            this.vnfPackages= vnfPackages;
    }

    @Override
    public void isValid() throws MalformattedElementException {
        log.debug("Entered into isValid method of OnBoardRequest");
       if(gst==null) {
            log.debug("GST inserted are null");
            throw new MalformattedElementException("On board NS Template request without NS Template");
        }else if(gst!=null) {
            log.debug("gst.isValid throws an exception");
            gst.isValid();

            if (nsds!=null && !nsds.isEmpty()){
                for (Nsd nsd : nsds) nsd.isValid();
            }

            if(vnfPackages!=null && !vnfPackages.isEmpty()){
                for (OnBoardVnfPackageRequest vnf : vnfPackages) vnf.isValid();
            }
        }

        if(pnfds != null && !pnfds.isEmpty()){
            for (Pnfd pnfd : pnfds) pnfd.isValid();
        }
        if(!configurationRules.isEmpty()) for (NstConfigurationRule r : configurationRules) r.isValid();
    }

    public void setGst(GST gst) {
        this.gst = gst;
    }

    public GST getGst() {
        return gst;
    }

    public String getNstId() {
        return nstId;
    }

    public void setNstId(String nstId) {
        this.nstId = nstId;
    }

    public List<Nsd> getNsds() {
        return nsds;
    }

    public List<Pnfd> getPnfds() { return pnfds; }

    /**
     * @return the vnfPackages
     */
    public List<OnBoardVnfPackageRequest> getVnfPackages() {
        return vnfPackages;
    }
}
