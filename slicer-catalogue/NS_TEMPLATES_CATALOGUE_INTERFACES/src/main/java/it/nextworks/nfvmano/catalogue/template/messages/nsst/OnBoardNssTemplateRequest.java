package it.nextworks.nfvmano.catalogue.template.messages.nsst;

import it.nextworks.nfvmano.catalogue.template.messages.nst.OnBoardNsTemplateRequest;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.OnBoardVnfPackageRequest;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.descriptors.nsd.Nsd;
import it.nextworks.nfvmano.libs.ifa.descriptors.nsd.Pnfd;
import it.nextworks.nfvmano.libs.ifa.templates.nst.NSST;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class OnBoardNssTemplateRequest {
    private static final Logger log = LoggerFactory.getLogger(OnBoardNsTemplateRequest.class);

    private List<NSST> nssts;
    private List<Nsd> nsds = new ArrayList<>();
    private List<OnBoardVnfPackageRequest> vnfPackages = new ArrayList<>();
    private List<Pnfd> pnfds = new ArrayList<>();

    public OnBoardNssTemplateRequest(){}

    public OnBoardNssTemplateRequest(List<NSST> nssts, List<Nsd> nsds, List<OnBoardVnfPackageRequest> vnfPackages, List<Pnfd> pnfds) {
        if(this.nsds!=null)
            this.nsds = nsds;
        this.nssts=nssts;
        if(vnfPackages!=null)
            this.vnfPackages= vnfPackages;
        if(pnfds!=null)
            this.pnfds = pnfds;
    }

    public void isValid() throws MalformattedElementException{
        if(nssts==null)
            throw new MalformattedElementException("NSST list cannot be null");
        for(NSST nsst: nssts)
            nsst.isValid();
    }

    public List<NSST> getNssts() {
        return nssts;
    }

    public void setNssts(List<NSST> nssts) {
        this.nssts = nssts;
    }

    public List<Nsd> getNsds() {
        return nsds;
    }

    public void setNsds(List<Nsd> nsds) {
        this.nsds = nsds;
    }

    public List<OnBoardVnfPackageRequest> getVnfPackages() {
        return vnfPackages;
    }

    public void setVnfPackages(List<OnBoardVnfPackageRequest> vnfPackages) {
        this.vnfPackages = vnfPackages;
    }

    public List<Pnfd> getPnfds() {
        return pnfds;
    }

    public void setPnfds(List<Pnfd> pnfds) {
        this.pnfds = pnfds;
    }
}
