package it.nextworks.nfvmano.catalogue.template.messages.nsst;

import it.nextworks.nfvmano.catalogue.template.elements.NssTemplateInfo;
import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

import java.util.ArrayList;
import java.util.List;

public class QueryNssTemplateResponse implements InterfaceMessage {

    private List<NssTemplateInfo> nssTemplateInfos = new ArrayList<>();

    public QueryNssTemplateResponse(){}

    /**
     * Constructor
     * @param nsTemplateInfos List of NSTemplate info
     */
    public QueryNssTemplateResponse(List<NssTemplateInfo> nsTemplateInfos) {
        this.nssTemplateInfos = nsTemplateInfos;
    }

    public void setNssTemplateInfos(List<NssTemplateInfo> nssTemplateInfos) {
        this.nssTemplateInfos = nssTemplateInfos;
    }

    /**
     * @return the nsTemplateInfo
     */
    public List<NssTemplateInfo> getNssTemplateInfos() {
        return nssTemplateInfos;
    }

    @Override
    public void isValid() throws MalformattedElementException {
        if(nssTemplateInfos !=null)
            for (NssTemplateInfo nstInfo : nssTemplateInfos) nstInfo.isValid();
        else
            throw new MalformattedElementException("NsTemplateInfos value is required");
    }
}
