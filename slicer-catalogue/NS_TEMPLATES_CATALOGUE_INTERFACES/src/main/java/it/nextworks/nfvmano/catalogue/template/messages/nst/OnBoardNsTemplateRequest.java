/*
 * Copyright (c) 2019 Nextworks s.r.l
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.nextworks.nfvmano.catalogue.template.messages.nst;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.nextworks.nfvmano.catalogue.template.elements.NstConfigurationRule;
import it.nextworks.nfvmano.libs.ifa.descriptors.nsd.Nsd;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.OnBoardVnfPackageRequest;
import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.descriptors.nsd.Pnfd;
import it.nextworks.nfvmano.libs.ifa.templates.nst.NST;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OnBoardNsTemplateRequest implements InterfaceMessage {

	private static final Logger log = LoggerFactory.getLogger(OnBoardNsTemplateRequest.class);

    private NST nst;
    private List<Nsd> nsds = new ArrayList<>();
	private List<OnBoardVnfPackageRequest> vnfPackages = new ArrayList<>();
	private List<Pnfd> pnfds = new ArrayList<>();

	private List<NstConfigurationRule> configurationRules = new ArrayList<>();

    public OnBoardNsTemplateRequest() { }

    /**
	 * Constructor
	 * @param nst the network slice template
	 * @param nsds a list of network slice descriptor
	 * @param vnfPackages a list of VNF packages
	 */
	public OnBoardNsTemplateRequest(NST nst, List<Nsd> nsds, List<OnBoardVnfPackageRequest> vnfPackages, List<Pnfd> pnfds, List<NstConfigurationRule> configurationRules) {
    	if(this.nsds!=null)
			this.nsds = nsds;
        this.nst = nst;
		if(vnfPackages!=null)
			this.vnfPackages= vnfPackages;
		if(pnfds!=null)
			this.pnfds = pnfds;
		if (configurationRules != null) this.configurationRules = configurationRules;
    }

    @Override
	public void isValid() throws MalformattedElementException {
    	log.debug("Entered into isValid method of OnBoardRequest");
		if (nst == null) {
			log.debug("NST inserted are null");
			throw new MalformattedElementException("On board NS Template request without NS Template");
		}else{
			log.debug("nst.isValid throws an exception");
			nst.isValid();

			if (nsds!=null && !nsds.isEmpty()){
				for (Nsd nsd : nsds) nsd.isValid();
			}

			if(vnfPackages!=null && !vnfPackages.isEmpty()){
				for (OnBoardVnfPackageRequest vnf : vnfPackages) vnf.isValid();
			}
		}

        if(pnfds != null && !pnfds.isEmpty()){
        	for (Pnfd pnfd : pnfds) pnfd.isValid();
		}
		if(!configurationRules.isEmpty()) for (NstConfigurationRule r : configurationRules) r.isValid();
	}

	public NST getNst() {
		return nst;
	}

	public void setNst(NST nst) {
		this.nst = nst;
	}

	public List<Nsd> getNsds() {
		return nsds;
	}

	public List<Pnfd> getPnfds() { return pnfds; }

	/**
	 * @return the vnfPackages
	 */
	public List<OnBoardVnfPackageRequest> getVnfPackages() {
		return vnfPackages;
	}

	public List<NstConfigurationRule> getConfigurationRules() {
		return configurationRules;
	}

	@JsonIgnore
	public void setNstIdInConfigurationRules(String nstId) {
		for (NstConfigurationRule cr : configurationRules)
			cr.setNstId(nstId);
	}
}
