/*
 * Copyright (c) 2021 Nextworks s.r.l
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.nfvmano.catalogues.template.services;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.nextworks.nfvmano.catalogue.template.elements.GsTemplateInfo;
import it.nextworks.nfvmano.catalogue.template.interfaces.NestCatalogueInterface;
import it.nextworks.nfvmano.catalogue.template.messages.nest.OnBoardNesTemplateRequest;
import it.nextworks.nfvmano.catalogue.template.messages.nest.QueryNesTemplateResponse;
import it.nextworks.nfvmano.catalogues.template.repo.GsTemplateInfoRepository;
import it.nextworks.nfvmano.catalogues.template.repo.GsTemplateRepository;
import it.nextworks.nfvmano.libs.ifa.common.elements.Filter;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.*;
import it.nextworks.nfvmano.libs.ifa.common.messages.GeneralizedQueryRequest;
import it.nextworks.nfvmano.libs.ifa.templates.gst.GST;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
public class NestCatalogueService implements NestCatalogueInterface {

    private static final Logger log = LoggerFactory.getLogger(NestCatalogueService.class);

    /*@Autowired
    private NfvoCatalogueService nfvoCatalogueService;*/

    @Autowired
    private GsTemplateRepository gstRepository;

    @Autowired
    private GsTemplateInfoRepository gstInfoRepository;

    public NestCatalogueService(){}

    @Override
    public synchronized String onBoardNesTemplate(OnBoardNesTemplateRequest request)
            throws MethodNotImplementedException, MalformattedElementException, AlreadyExistingEntityException, FailedOperationException{
        log.debug("Processing request to onboard a new NesTemplate");
        request.isValid();

        log.debug("Processing NFV descriptors");
        try {
            /*if (request.getVnfPackages() != null) {

                log.debug("Storing VNF packages");
                List<OnBoardVnfPackageRequest> vnfPackages = request.getVnfPackages();
                for (OnBoardVnfPackageRequest vnfR : vnfPackages) {
                    try {
                        OnBoardVnfPackageResponse vnfReply = nfvoCatalogueService.onBoardVnfPackage(vnfR);
                        String vnfPackageId = vnfReply.getOnboardedVnfPkgInfoId();
                        log.debug("Added VNF package for VNF " + vnfR.getName() +
                                ", version " + vnfR.getVersion() + ", provider " + vnfR.getProvider() + " in NFVO catalogue. VNF package ID: " + vnfPackageId);
                        //nsTemplateInfo.addVnfPackageInfoId(vnfPackageId);
                    } catch (AlreadyExistingEntityException e) {
                        log.debug("The VNF package is already present in the NFVO catalogue. Retrieving its ID.");
                        QueryOnBoardedVnfPkgInfoResponse r =
                                nfvoCatalogueService.queryVnfPackageInfo(new GeneralizedQueryRequest(TemplateCatalogueUtilities.buildVnfPackageInfoFilter(vnfR.getName(), vnfR.getVersion(), vnfR.getProvider()), null));
                        String oldVnfPackageId = r.getQueryResult().get(0).getOnboardedVnfPkgInfoId();
                        log.debug("Retrieved VNF package ID: " + oldVnfPackageId);
                        //nsTemplateInfo.addVnfPackageInfoId(oldVnfPackageId);
                    }
                }
            }
            if (request.getNsds() != null) {
                log.debug("Storing NSDs");
                List<Nsd> nsds = request.getNsds();
                for (Nsd nsd : nsds) {
                    try {
                        Map<String, String> userDefinedData = new HashMap<>();
                        String nsdInfoId = nfvoCatalogueService.onboardNsd(new OnboardNsdRequest(nsd, userDefinedData));
                        log.debug("Added NSD " + nsd.getNsdIdentifier() +
                                ", version " + nsd.getVersion() + " in NFVO catalogue. NSD Info ID: " + nsdInfoId);
                        //nsTemplateInfo.addNsdInfoId(nsdInfoId);
                    } catch (AlreadyExistingEntityException e) {
                        log.debug("The NSD is already present in the NFVO catalogue. Retrieving its ID.");
                        QueryNsdResponse nsdR = nfvoCatalogueService.queryNsd(new GeneralizedQueryRequest(TemplateCatalogueUtilities.buildNsdInfoFilter(nsd.getNsdIdentifier(), nsd.getVersion()), null));
                        String oldNsdInfoId = nsdR.getQueryResult().get(0).getNsdInfoId();
                        log.debug("Retrieved NSD Info ID: " + oldNsdInfoId);
                        //nsTemplateInfo.addNsdInfoId(oldNsdInfoId);
                    }
                }
            }
            if (request.getPnfds() != null) {
                log.debug("Storing Pnfds");
                List<Pnfd> pnfds = request.getPnfds();
                for (Pnfd pnfd : pnfds) {
                    try {
                        Map<String, String> userDefinedData = new HashMap<>();
                        String pnfdInfoId = nfvoCatalogueService.onboardPnfd(new OnboardPnfdRequest(pnfd, userDefinedData));
                        log.debug("Added PNFD " + pnfd.getPnfdId() +
                                ", version " + pnfd.getVersion() + " in NFVO catalogue. NSD Info ID: " + pnfdInfoId);
                        //nsTemplateInfo.addVnfPackageInfoId(vnfPackageId);
                    } catch (AlreadyExistingEntityException e) {
                        log.debug("The PNFD is already present in the NFVO catalogue. Retrieving its ID.");
                        QueryPnfdResponse queryPnfdResponse = nfvoCatalogueService.queryPnfd(new GeneralizedQueryRequest(TemplateCatalogueUtilities.buildPnfdInfoFilter(pnfd.getPnfdId(), pnfd.getVersion()), null));
                        String oldPnfdInfoId = queryPnfdResponse.getQueryResult().get(0).getPnfdInfoId();
                        log.debug("Retrieved PNFD Info ID: " + oldPnfdInfoId);
                        //nsTemplateInfo.addVnfPackageInfoId(oldVnfPackageId);
                    }
                }
            }*/
            String gstId = storeGsTemplate(request.getGst(), request.getNstId());

            return gstId;
        }catch (Exception e) {
            log.error("Something went wrong when processing SO descriptors.",e );
            throw new FailedOperationException("Internal error: something went wrong when processing VNF packages / NS descriptors.", e);
        }
    }

    private GsTemplateInfo getGsTemplateInfo(String gstId) throws NotExistingEntityException {
        if(!gstInfoRepository.findByGsTemplateId(gstId).isPresent())
            throw new NotExistingEntityException("GsTemplate info for NsTemplate with ID " + gstId + " not found in DB.");

        GsTemplateInfo gstInfo=gstInfoRepository.findByGsTemplateId(gstId).get();
        if(gstRepository.findByGstId(gstId).isPresent()) {
            GST gst = getGsTemplate(gstId);
            gstInfo.setGst(gst);
        }
        return gstInfo;
    }

    private GsTemplateInfo getGsTemplateInfo(String name, String version) throws NotExistingEntityException{
        if(!gstInfoRepository.findByNameAndGsTemplateVersion(name, version).isPresent())
            throw new NotExistingEntityException("GsTemplate info for NsTemplate with name " + name + " and version "+ version +"not found in DB.");

        GsTemplateInfo gstInfo=gstInfoRepository.findByNameAndGsTemplateVersion(name, version).get();
        if(gstRepository.findByGstNameAndGstVersion(name, version).isPresent()) {
            GST gst = getGsTemplate(name, version);
            gstInfo.setGst(gst);
        }
        return gstInfo;
    }

    private GST getGsTemplate(String gstID) throws NotExistingEntityException{
        if (gstRepository.findByGstId(gstID).isPresent())
            return gstRepository.findByGstId(gstID).get();
        else
            throw new NotExistingEntityException("GsTemplate with ID " + gstID + " not found in DB.");
    }

    private GST getGsTemplate(String name, String version) throws NotExistingEntityException{
        if (gstRepository.findByGstNameAndGstVersion(name, version).isPresent())
            return gstRepository.findByGstNameAndGstVersion(name, version).get();
        else
            throw new NotExistingEntityException("GsTemplate with name " + name + " and version " + version + " not found in DB.");
    }

    private List<GsTemplateInfo> getAllGsTemplateInfos() throws NotExistingEntityException{
        List<GsTemplateInfo> gstInfos =gstInfoRepository.findAll();
        for(GsTemplateInfo gstInfo: gstInfos){
            String name=gstInfo.getName();
            String version=gstInfo.getGsTemplateVersion();
            if(gstRepository.findByGstNameAndGstVersion(name,version).isPresent()){
                GST gst=getGsTemplate(name,version);
                gstInfo.setGst(gst);
            }
        }
        return gstInfos;
    }

    public QueryNesTemplateResponse queryNesTemplate(GeneralizedQueryRequest request) throws MethodNotImplementedException, MalformattedElementException, NotExistingEntityException, FailedOperationException {
        log.debug("Processing request to query a NesTemplate");
        request.isValid();
        List<GsTemplateInfo> gstInfos = new ArrayList<>();

        Filter filter = request.getFilter();
        List<String> attributeSelector = request.getAttributeSelector();
        if ((attributeSelector == null) || (attributeSelector.isEmpty())) {
            Map<String, String> filterPars = filter.getParameters();

            if (filterPars.size() == 1 && filterPars.containsKey("NST_ID")) {
                String nstId = filterPars.get("NST_ID");
                GsTemplateInfo nestInfo = getGsTemplateInfo(nstId);
                gstInfos.add(nestInfo);
                log.debug("Added NsTemplate info for NsTemplate ID " + nstId);
            } else if (filterPars.size() == 2 && filterPars.containsKey("NST_NAME") && filterPars.containsKey("NST_VERSION")) {
                String nstName = filterPars.get("NST_NAME");
                String nstVersion = filterPars.get("NST_VERSION");
                GsTemplateInfo nestInfo = getGsTemplateInfo(nstName, nstVersion);
                gstInfos.add(nestInfo);
                log.debug("Added NsTemplate info for NsTemplate with name " + nstName + " and version " + nstVersion);
            } else if (filterPars.isEmpty()) {
                gstInfos = getAllGsTemplateInfos();
                log.debug("Addes all the NTSs info available in DB.");
            }
            return new QueryNesTemplateResponse(gstInfos);
        } else {
            String logErrorStr = "Received query NsTemplate with attribute selector. Not supported at the moment.";
            log.error(logErrorStr);
            throw new MethodNotImplementedException(logErrorStr);
        }
    }

    @Override
    public synchronized void deleteNesTemplate(String nesTemplateId) throws MethodNotImplementedException, MalformattedElementException, NotExistingEntityException, FailedOperationException{
        log.debug("Processing request to delete a NesTemplate with ID " + nesTemplateId);
        if (nesTemplateId == null)
            throw new MalformattedElementException("The NsTemplate ID is null");

        GsTemplateInfo nestInfo = getGsTemplateInfo(nesTemplateId);
        if(gstRepository.findByGstId(nesTemplateId).isPresent()) {
            GST gst= getGsTemplate(nesTemplateId);
            gstRepository.delete(gst);
            log.debug("Removed GsTemplate from DB.");
        }
        gstInfoRepository.delete(nestInfo);
    }

    private String storeGsTemplate(GST gst, String nstId) throws AlreadyExistingEntityException {
        String gstVersion =gst.getGstVersion();
        String gstTargetName =gst.getGstName();
        String gstTargetId = gst.getGstId();
        log.debug("Onboarding NsTemplate with name " + gstTargetName + " and version " + gstVersion);

        if (gstRepository.findByGstNameAndGstVersion(gstTargetName, gstVersion).isPresent()
                || gstRepository.findByGstId(gstTargetId).isPresent()) {
            String logErrorStr= "NsTemplate with name " + gstTargetName + " and version " + gstVersion + " or ID already present in DB.";
            log.error(logErrorStr);
            throw new AlreadyExistingEntityException(logErrorStr);
        }

        ObjectMapper mapper=new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        GST target=null;
        try {
            String nestAsString = mapper.writeValueAsString(gst);
            target=mapper.readValue(nestAsString, GST.class);
        }catch (IOException e){
            log.error("Impossible to create target copy of NEST with ID {}", gstTargetId);
        }
		/*GST target = new GST(gstTargetId,
				gstTargetName,
				gst.getGstProvider(),
				gstVersion
				);*/

        //We save the reference passed in the method invocation, we are not sure that it is permitted,
        //in case something goes wrong during tests we have to create a copy (target above) and save it
        gstRepository.saveAndFlush(target);
        log.debug("Added NsTemplate with ID " + gstTargetId);

        GsTemplateInfo gstInfo = new GsTemplateInfo(UUID.randomUUID().toString(), gstTargetId, gstTargetName, gstVersion, gst.getGstProvider(), target, null, null,false);
        if(nstId!=null)
            gstInfo.setReferenceNstId(nstId);
        gstInfoRepository.saveAndFlush(gstInfo);

        log.debug("Added NsTemplate Info with NsTemplate ID " + gstTargetId);
        return gstTargetId;
    }
}
