/*
 * Copyright (c) 2021 Nextworks s.r.l
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.nfvmano.catalogues.template.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.nextworks.nfvmano.catalogue.template.elements.NssTemplateInfo;
import it.nextworks.nfvmano.catalogue.template.messages.nsst.OnBoardNssTemplateRequest;
import it.nextworks.nfvmano.catalogue.template.messages.nsst.QueryNssTemplateResponse;
import it.nextworks.nfvmano.catalogues.template.repo.*;
import it.nextworks.nfvmano.catalogue.template.elements.NstConfigurationRule;
import it.nextworks.nfvmano.libs.ifa.templates.nst.NSST;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.nextworks.nfvmano.catalogue.template.elements.NsTemplateInfo;
import it.nextworks.nfvmano.catalogue.template.interfaces.NsTemplateCatalogueInterface;
import it.nextworks.nfvmano.catalogue.template.messages.nst.OnBoardNsTemplateRequest;
import it.nextworks.nfvmano.catalogue.template.messages.nst.QueryNsTemplateResponse;
import it.nextworks.nfvmano.libs.ifa.common.elements.Filter;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.*;
import it.nextworks.nfvmano.libs.ifa.common.messages.GeneralizedQueryRequest;
import it.nextworks.nfvmano.libs.ifa.templates.nst.NST;
import org.springframework.stereotype.Service;
import java.util.HashMap;

@Service
public class NsTemplateCatalogueService implements NsTemplateCatalogueInterface {

	private static final Logger log = LoggerFactory.getLogger(NsTemplateCatalogueService.class);

	/*@Autowired
	private NfvoCatalogueService nfvoCatalogueService;*/

	@Autowired
	private NsTemplateInfoRepository nstInfoRepository;

	@Autowired
	private NsTemplateRepository nstRepository;

	@Autowired
	private NssTemplateRepository nsstRepository;

	@Autowired
	private NssTemplateInfoRepository nsstInfoRepository;

	@Autowired
	private ConfigurationRuleRepository configurationRuleRepository;

	public NsTemplateCatalogueService() { }

	public static Filter buildVnfPackageInfoFilter(String vnfProductName, String swVersion, String provider) {
		//VNF_PACKAGE_PRODUCT_NAME
		//VNF_PACKAGE_SW_VERSION
		//VNF_PACKAGE_PROVIDER
		Map<String, String> filterParams = new HashMap<>();
		filterParams.put("VNF_PACKAGE_PRODUCT_NAME", vnfProductName);
		filterParams.put("VNF_PACKAGE_SW_VERSION", swVersion);
		filterParams.put("VNF_PACKAGE_PROVIDER", provider);
		return new Filter(filterParams);
	}
	public static Filter buildNsdInfoFilter(String nsdId, String nsdVersion) {
		//NSD_ID
		//NSD_VERSION
		Map<String, String> filterParams = new HashMap<>();
		filterParams.put("NSD_ID", nsdId);
		filterParams.put("NSD_VERSION", nsdVersion);
		return new Filter(filterParams);
	}
	@Override
	public synchronized String onBoardNsTemplate(OnBoardNsTemplateRequest request)
			throws MethodNotImplementedException, MalformattedElementException, AlreadyExistingEntityException, FailedOperationException {
		log.debug("Processing request to onboard a new NsTemplate");
		request.isValid();
		//we have to add also nsd and vnf packages to store

		log.debug("Processing NFV descriptors");
		try {
			/*if(request.getVnfPackages()!=null) {

				log.debug("Storing VNF packages");
				List<OnBoardVnfPackageRequest> vnfPackages = request.getVnfPackages();
				for (OnBoardVnfPackageRequest vnfR : vnfPackages) {
					try {
						OnBoardVnfPackageResponse vnfReply = nfvoCatalogueService.onBoardVnfPackage(vnfR);
						String vnfPackageId = vnfReply.getOnboardedVnfPkgInfoId();
						log.debug("Added VNF package for VNF " + vnfR.getName() +
								", version " + vnfR.getVersion() + ", provider " + vnfR.getProvider() + " in NFVO catalogue. VNF package ID: " + vnfPackageId);
						//nsTemplateInfo.addVnfPackageInfoId(vnfPackageId);
					} catch (AlreadyExistingEntityException e) {
						log.debug("The VNF package is already present in the NFVO catalogue. Retrieving its ID.");
						QueryOnBoardedVnfPkgInfoResponse r =
								nfvoCatalogueService.queryVnfPackageInfo(new GeneralizedQueryRequest(TemplateCatalogueUtilities.buildVnfPackageInfoFilter(vnfR.getName(), vnfR.getVersion(), vnfR.getProvider()), null));
						String oldVnfPackageId = r.getQueryResult().get(0).getOnboardedVnfPkgInfoId();
						log.debug("Retrieved VNF package ID: " + oldVnfPackageId);
						//nsTemplateInfo.addVnfPackageInfoId(oldVnfPackageId);
					}
				}
			}
			if(request.getNsds()!=null) {
				log.debug("Storing NSDs");
				List<Nsd> nsds = request.getNsds();
				for (Nsd nsd : nsds) {
					try {
						Map<String, String> userDefinedData = new HashMap<>();
						String nsdInfoId = nfvoCatalogueService.onboardNsd(new OnboardNsdRequest(nsd, userDefinedData));
						log.debug("Added NSD " + nsd.getNsdIdentifier() +
								", version " + nsd.getVersion() + " in NFVO catalogue. NSD Info ID: " + nsdInfoId);
						//nsTemplateInfo.addNsdInfoId(nsdInfoId);
					} catch (AlreadyExistingEntityException e) {
						log.debug("The NSD is already present in the NFVO catalogue. Retrieving its ID.");
						QueryNsdResponse nsdR = nfvoCatalogueService.queryNsd(new GeneralizedQueryRequest(TemplateCatalogueUtilities.buildNsdInfoFilter(nsd.getNsdIdentifier(), nsd.getVersion()), null));
						String oldNsdInfoId = nsdR.getQueryResult().get(0).getNsdInfoId();
						log.debug("Retrieved NSD Info ID: " + oldNsdInfoId);
						//nsTemplateInfo.addNsdInfoId(oldNsdInfoId);
					}
				}
			}
			if(request.getPnfds()!=null) {
				log.debug("Storing Pnfds");
				List<Pnfd> pnfds = request.getPnfds();
				for (Pnfd pnfd : pnfds) {
					try {
						Map<String, String> userDefinedData = new HashMap<>();
						String pnfdInfoId = nfvoCatalogueService.onboardPnfd(new OnboardPnfdRequest(pnfd,userDefinedData));
						log.debug("Added PNFD " + pnfd.getPnfdId() +
								", version " + pnfd.getVersion() + " in NFVO catalogue. NSD Info ID: " + pnfdInfoId);
						//nsTemplateInfo.addVnfPackageInfoId(vnfPackageId);
					} catch (AlreadyExistingEntityException e) {
						log.debug("The PNFD is already present in the NFVO catalogue. Retrieving its ID.");
						QueryPnfdResponse queryPnfdResponse = nfvoCatalogueService.queryPnfd(new GeneralizedQueryRequest(TemplateCatalogueUtilities.buildPnfdInfoFilter(pnfd.getPnfdId(),pnfd.getVersion()),null));
						String oldPnfdInfoId = queryPnfdResponse.getQueryResult().get(0).getPnfdInfoId();
						log.debug("Retrieved PNFD Info ID: " + oldPnfdInfoId);
						//nsTemplateInfo.addVnfPackageInfoId(oldVnfPackageId);
					}
				}
			}*/
			String nstId = storeNsTemplate(request.getNst());

			request.setNstIdInConfigurationRules(nstId);
			log.debug("Storing configuration rules");
			List<NstConfigurationRule> crs = request.getConfigurationRules();
			for (NstConfigurationRule cr : crs) {
				cr.isValid();
				configurationRuleRepository.saveAndFlush(cr);
			}
			log.debug("Configuration rules saved in internal DB.");
			return nstId;
		}catch (Exception e) {
			log.error("Something went wrong when processing SO descriptors.",e );
			throw new FailedOperationException("Internal error: something went wrong when processing VNF packages / NS descriptors.", e);
		}
	}

	public synchronized List<String> onBoardNssTemplate(OnBoardNssTemplateRequest request)
			throws MalformattedElementException, AlreadyExistingEntityException, FailedOperationException{
		request.isValid();

		try {
			/*if(request.getVnfPackages()!=null) {

				log.debug("Storing VNF packages");
				List<OnBoardVnfPackageRequest> vnfPackages = request.getVnfPackages();
				for (OnBoardVnfPackageRequest vnfR : vnfPackages) {
					try {
						OnBoardVnfPackageResponse vnfReply = nfvoCatalogueService.onBoardVnfPackage(vnfR);
						String vnfPackageId = vnfReply.getOnboardedVnfPkgInfoId();
						log.debug("Added VNF package for VNF " + vnfR.getName() +
								", version " + vnfR.getVersion() + ", provider " + vnfR.getProvider() + " in NFVO catalogue. VNF package ID: " + vnfPackageId);
						//nsTemplateInfo.addVnfPackageInfoId(vnfPackageId);
					} catch (AlreadyExistingEntityException e) {
						log.debug("The VNF package is already present in the NFVO catalogue. Retrieving its ID.");
						QueryOnBoardedVnfPkgInfoResponse r =
								nfvoCatalogueService.queryVnfPackageInfo(new GeneralizedQueryRequest(TemplateCatalogueUtilities.buildVnfPackageInfoFilter(vnfR.getName(), vnfR.getVersion(), vnfR.getProvider()), null));
						String oldVnfPackageId = r.getQueryResult().get(0).getOnboardedVnfPkgInfoId();
						log.debug("Retrieved VNF package ID: " + oldVnfPackageId);
						//nsTemplateInfo.addVnfPackageInfoId(oldVnfPackageId);
					}
				}
			}
			if(request.getNsds()!=null) {
				log.debug("Storing NSDs");
				List<Nsd> nsds = request.getNsds();
				for (Nsd nsd : nsds) {
					try {
						Map<String, String> userDefinedData = new HashMap<>();
						String nsdInfoId = nfvoCatalogueService.onboardNsd(new OnboardNsdRequest(nsd, userDefinedData));
						log.debug("Added NSD " + nsd.getNsdIdentifier() +
								", version " + nsd.getVersion() + " in NFVO catalogue. NSD Info ID: " + nsdInfoId);
						//nsTemplateInfo.addNsdInfoId(nsdInfoId);
					} catch (AlreadyExistingEntityException e) {
						log.debug("The NSD is already present in the NFVO catalogue. Retrieving its ID.");
						QueryNsdResponse nsdR = nfvoCatalogueService.queryNsd(new GeneralizedQueryRequest(TemplateCatalogueUtilities.buildNsdInfoFilter(nsd.getNsdIdentifier(), nsd.getVersion()), null));
						String oldNsdInfoId = nsdR.getQueryResult().get(0).getNsdInfoId();
						log.debug("Retrieved NSD Info ID: " + oldNsdInfoId);
						//nsTemplateInfo.addNsdInfoId(oldNsdInfoId);
					}
				}
			}
			if(request.getPnfds()!=null) {
				log.debug("Storing Pnfds");
				List<Pnfd> pnfds = request.getPnfds();
				for (Pnfd pnfd : pnfds) {
					try {
						Map<String, String> userDefinedData = new HashMap<>();
						String pnfdInfoId = nfvoCatalogueService.onboardPnfd(new OnboardPnfdRequest(pnfd,userDefinedData));
						log.debug("Added PNFD " + pnfd.getPnfdId() +
								", version " + pnfd.getVersion() + " in NFVO catalogue. NSD Info ID: " + pnfdInfoId);
						//nsTemplateInfo.addVnfPackageInfoId(vnfPackageId);
					} catch (AlreadyExistingEntityException e) {
						log.debug("The PNFD is already present in the NFVO catalogue. Retrieving its ID.");
						QueryPnfdResponse queryPnfdResponse = nfvoCatalogueService.queryPnfd(new GeneralizedQueryRequest(TemplateCatalogueUtilities.buildPnfdInfoFilter(pnfd.getPnfdId(),pnfd.getVersion()),null));
						String oldPnfdInfoId = queryPnfdResponse.getQueryResult().get(0).getPnfdInfoId();
						log.debug("Retrieved PNFD Info ID: " + oldPnfdInfoId);
						//nsTemplateInfo.addVnfPackageInfoId(oldVnfPackageId);
					}
				}
			}*/
			List<String> nsstIds=new ArrayList<>();
			for(NSST nsst: request.getNssts())
				nsstIds.add(storeNssTemplate(nsst));

			return nsstIds;
		}catch (Exception e) {
			log.error("Something went wrong when processing SO descriptors.",e );
			throw new FailedOperationException("Internal error: something went wrong when processing VNF packages / NS descriptors.", e);
		}
	}

	private NsTemplateInfo getNsTemplateInfo(String nstId) throws NotExistingEntityException {
		if(!nstInfoRepository.findByNsTemplateId(nstId).isPresent())
			throw new NotExistingEntityException("NsTemplate info for NsTemplate with ID " + nstId + " not found in DB.");

		NsTemplateInfo nstInfo=nstInfoRepository.findByNsTemplateId(nstId).get();
		//Control to determine if NsTemplateInfo contains NST or GST and then call the appropriate method
		if(nstRepository.findByNstId(nstId).isPresent()) {
			NST nst = getNsTemplate(nstId);
			nstInfo.setNST(nst);
		}
		return nstInfo;
	}

	private NssTemplateInfo getNssTemplateInfo(String nsstId) throws NotExistingEntityException {
		if (!nsstInfoRepository.findByNssTemplateId(nsstId).isPresent())
			throw new NotExistingEntityException("NssTemplate info for NsTemplate with ID " + nsstId + " not found in DB.");

		NssTemplateInfo nsstInfo=nsstInfoRepository.findByNssTemplateId(nsstId).get();
		if(nsstRepository.findByNsstId(nsstId).isPresent()){
			NSST nsst=getNssTemplate(nsstId);
			nsstInfo.setNsst(nsst);
		}
		return nsstInfo;
	}


	private NsTemplateInfo getNsTemplateInfo(String name, String version) throws NotExistingEntityException {
		if(!nstInfoRepository.findByNameAndNsTemplateVersion(name, version).isPresent())
			throw new NotExistingEntityException("NsTemplate info for NsTemplate with name " + name + " and version "+ version +"not found in DB.");

		NsTemplateInfo nstInfo=nstInfoRepository.findByNameAndNsTemplateVersion(name, version).get();
		//Control to determine if NsTemplateInfo contains NST or GST and then call the appropriate method
		if(nstRepository.findByNstNameAndNstVersion(name, version).isPresent()){
			NST nst=getNsTemplate(name,version);
			nstInfo.setNST(nst);
		}
		return nstInfo;
	}

	private NssTemplateInfo getNssTemplateInfo(String name, String version) throws NotExistingEntityException {
		if (!nstInfoRepository.findByNameAndNsTemplateVersion(name, version).isPresent())
			throw new NotExistingEntityException("NsTemplate info for NsTemplate with name " + name + " and version " + version + "not found in DB.");

		NssTemplateInfo nsstInfo=nsstInfoRepository.findByNameAndNssTemplateVersion(name, version).get();
		if(nsstRepository.findByNsstNameAndNsstVersion(name, version).isPresent()){
			NSST nsst=getNssTemplate(name, version);
			nsstInfo.setNsst(nsst);
		}
		return nsstInfo;
	}

	private NST getNsTemplate(String nstID) throws NotExistingEntityException{
		if (nstRepository.findByNstId(nstID).isPresent())
			return nstRepository.findByNstId(nstID).get();
		else
			throw new NotExistingEntityException("NsTemplate with ID " + nstID + " not found in DB.");
	}

	private NST getNsTemplate(String name, String version) throws NotExistingEntityException{
		if (nstRepository.findByNstNameAndNstVersion(name, version).isPresent())
			return nstRepository.findByNstNameAndNstVersion(name, version).get();
		else
			throw new NotExistingEntityException("NsTemplate with name " + name + " and version " + version + " not found in DB.");
	}

	private NSST getNssTemplate(String nsstID) throws NotExistingEntityException{
		if (nsstRepository.findByNsstId(nsstID).isPresent())
			return nsstRepository.findByNsstId(nsstID).get();
		else
			throw new NotExistingEntityException("NsTemplate with ID " + nsstID + " not found in DB.");
	}

	private NSST getNssTemplate(String name, String version) throws NotExistingEntityException{
		if (nsstRepository.findByNsstNameAndNsstVersion(name, version).isPresent())
			return nsstRepository.findByNsstNameAndNsstVersion(name, version).get();
		else
			throw new NotExistingEntityException("NsTemplate with name " + name + " and version " + version + " not found in DB.");
	}

	@Override
	public QueryNsTemplateResponse queryNsTemplate(GeneralizedQueryRequest request) throws MethodNotImplementedException, MalformattedElementException, NotExistingEntityException, FailedOperationException {
		log.debug("Processing request to query a NsTemplate");
		request.isValid();

		//At the moment the only filters accepted are:
		//1. NS Template name and version
		//NST_NAME & NST_VERSION
		//2. NS Template ID
		//NST_ID
		//No attribute selector is supported at the moment

		List<NsTemplateInfo> nstInfos = new ArrayList<>();

		List<NssTemplateInfo> nsstInfos = new ArrayList<>();

		Filter filter = request.getFilter();

		List<String> attributeSelector = request.getAttributeSelector();
		if ((attributeSelector == null) || (attributeSelector.isEmpty())) {
			Map<String, String> filterPars = filter.getParameters();

			if (filterPars.size() == 1 && filterPars.containsKey("NST_ID")) {
				String nstId = filterPars.get("NST_ID");
				NsTemplateInfo nstInfo = getNsTemplateInfo(nstId);
				nstInfos.add(nstInfo);
				log.debug("Added NsTemplate info for NsTemplate ID " + nstId);
			} else if (filterPars.size() == 2 && filterPars.containsKey("NST_NAME") && filterPars.containsKey("NST_VERSION")) {
				String nstName = filterPars.get("NST_NAME");
				String nstVersion = filterPars.get("NST_VERSION");
				NsTemplateInfo nstInfo = getNsTemplateInfo(nstName, nstVersion);
				nstInfos.add(nstInfo);
				log.debug("Added NsTemplate info for NsTemplate with name " + nstName + " and version " + nstVersion);
			} else if (filterPars.isEmpty()) {
				nstInfos = getAllNsTemplateInfos();
				log.debug("Addes all the NTSs info available in DB.");
			}
			return new QueryNsTemplateResponse(nstInfos);
		} else {
			String logErrorStr = "Received query NsTemplate with attribute selector. Not supported at the moment.";
			log.error(logErrorStr);
			throw new MethodNotImplementedException(logErrorStr);
		}
    }

	public QueryNssTemplateResponse queryNssTemplate(GeneralizedQueryRequest request) throws MethodNotImplementedException, MalformattedElementException, NotExistingEntityException, FailedOperationException {
		log.debug("Processing request to query a NsTemplate");
		request.isValid();

		//At the moment the only filters accepted are:
		//1. NS Template name and version
		//NST_NAME & NST_VERSION
		//2. NS Template ID
		//NST_ID
		//No attribute selector is supported at the moment

		List<NssTemplateInfo> nsstInfos = new ArrayList<>();

		Filter filter = request.getFilter();

		List<String> attributeSelector = request.getAttributeSelector();
		if ((attributeSelector == null) || (attributeSelector.isEmpty())) {
			Map<String, String> filterPars = filter.getParameters();

			if (filterPars.size() == 1 && filterPars.containsKey("NSST_ID")) {
				String nsstId = filterPars.get("NSST_ID");
				NssTemplateInfo nsstInfo = getNssTemplateInfo(nsstId);
				nsstInfos.add(nsstInfo);
				log.debug("Added NsTemplate info for NsTemplate ID " + nsstId);
			} else if (filterPars.size() == 2 && filterPars.containsKey("NST_NAME") && filterPars.containsKey("NST_VERSION")) {
				String nstName = filterPars.get("NST_NAME");
				String nstVersion = filterPars.get("NST_VERSION");
				NssTemplateInfo nsstInfo = getNssTemplateInfo(nstName, nstVersion);
				nsstInfos.add(nsstInfo);
				log.debug("Added NsTemplate info for NsTemplate with name " + nstName + " and version " + nstVersion);
			} else if (filterPars.isEmpty()) {
				nsstInfos = getAllNssTemplateInfos();
				log.debug("Addes all the NTSs info available in DB.");
			}
			return new QueryNssTemplateResponse(nsstInfos);
		} else {
			String logErrorStr = "Received query NsTemplate with attribute selector. Not supported at the moment.";
			log.error(logErrorStr);
			throw new MethodNotImplementedException(logErrorStr);
		}
	}

    
	private List<NsTemplateInfo> getAllNsTemplateInfos() throws NotExistingEntityException {
		List<NsTemplateInfo> nstInfos = nstInfoRepository.findAll();
		for (NsTemplateInfo nstInfo : nstInfos) {
			String name = nstInfo.getName();
			String version = nstInfo.getNsTemplateVersion();
			if(nstRepository.findByNstNameAndNstVersion(name, version).isPresent()){
				NST nst = getNsTemplate(name, version);
				nstInfo.setNST(nst);
			}
		}
		return nstInfos;
	}



	private List<NssTemplateInfo> getAllNssTemplateInfos() throws NotExistingEntityException{
		List<NssTemplateInfo> nsstInfos=nsstInfoRepository.findAll();
		for(NssTemplateInfo nsstInfo: nsstInfos){
			String name=nsstInfo.getName();
			String version=nsstInfo.getNssTemplateVersion();
			if(nsstRepository.findByNsstNameAndNsstVersion(name, version).isPresent()){
				NSST nsst= getNssTemplate(name, version);
				nsstInfo.setNsst(nsst);
			}
		}

		return nsstInfos;
	}
	
    @Override
    public synchronized void deleteNsTemplate(String nsTemplateId) throws MethodNotImplementedException, MalformattedElementException, NotExistingEntityException, FailedOperationException {
    	log.debug("Processing request to delete a NsTemplate with ID " + nsTemplateId);
    	if (nsTemplateId == null)
			throw new MalformattedElementException("The NsTemplate ID is null");

		NsTemplateInfo nstInfo = getNsTemplateInfo(nsTemplateId);
		NST nst = getNsTemplate(nsTemplateId);
		deleteNssTemplate(nst.getNsst().getNsstId());
		nstRepository.delete(nst);
		log.debug("Removed NsTemplate from DB.");
		nstInfoRepository.delete(nstInfo);
		log.debug("Removed NsTemplate info from DB.");
    }

	public synchronized void deleteNssTemplate(String nssTemplateId) throws MethodNotImplementedException, MalformattedElementException, NotExistingEntityException, FailedOperationException {
		log.debug("Processing request to delete a NsTemplate with ID " + nssTemplateId);
		if (nssTemplateId == null)
			throw new MalformattedElementException("The NssTemplate ID is null");

		NssTemplateInfo nsstInfo=getNssTemplateInfo(nssTemplateId);
		NSST nsst= getNssTemplate(nssTemplateId);
		for(NSST n: nsst.getNsstList())
			deleteNssTemplate(n.getNsstId());
		nsstRepository.delete(nsst);
		log.debug("Remove NssTemplate from DB");
		nsstInfoRepository.delete(nsstInfo);
		log.debug("Removed NssTemplate info from DB.");
	}
    


	private String storeNsTemplate(NST nst) throws AlreadyExistingEntityException {
		String nstVersion =nst.getNstVersion();
		String nstTargetName =nst.getNstName();
		String nstTargetId = nst.getNstId();
		log.debug("Onboarding NsTemplate with name " + nstTargetName + " and version " + nstVersion);

		if (nstRepository.findByNstNameAndNstVersion(nstTargetName, nstVersion).isPresent()
				|| nstRepository.findByNstId(nstTargetId).isPresent()) {
			String logErrorStr= "NsTemplate with name " + nstTargetName + " and version " + nstVersion + " or ID already present in DB.";
			log.error(logErrorStr);
			throw new AlreadyExistingEntityException(logErrorStr);
		}

		NSST nested_nsst=nst.getNsst();
		String nsstId;
		NST target;
		if(nested_nsst!=null) {
			nsstId = storeNssTemplate(nested_nsst);
			//We have to use a copy of the original NST because it causes problems with the DB
			target = new NST(nstTargetId,
						nstTargetName,
						nstVersion,
						nst.getNstProvider(),
						nst.getAdministrativeState(),
						nst.getNstServiceProfileList(),
						nsstRepository.findByNsstId(nsstId).get());
		}else{
			target = new NST(nstTargetId,
					nstTargetName,
					nstVersion,
					nst.getNstProvider(),
					nst.getAdministrativeState(),
					nst.getNstServiceProfileList(),
					null);
		}

		nstRepository.saveAndFlush(target);
    	log.debug("Added NsTemplate with ID " + nstTargetId);
    	
    	NsTemplateInfo nstInfo = new NsTemplateInfo(UUID.randomUUID().toString(), nstTargetId, nstTargetName, nstVersion, nst.getNstProvider(), target,null, null,false);
		nstInfoRepository.saveAndFlush(nstInfo);
		
		log.debug("Added NsTemplate Info with NsTemplate ID " + nstTargetId);
    	return nstTargetId;
    }

	private String storeNssTemplate(NSST nsst) throws AlreadyExistingEntityException{
		String nsstVersion =nsst.getNsstVersion();
		String nsstTargetName =nsst.getNsstName();
		String nsstTargetId = nsst.getNsstId();
		log.debug("Onboarding NssTemplate with name " + nsstTargetName + " and version " + nsstVersion);

		if (nsstRepository.findByNsstNameAndNsstVersion(nsstTargetName, nsstVersion).isPresent()
				|| nsstRepository.findByNsstId(nsstTargetId).isPresent()) {
			String logErrorStr= "NssTemplate with name " + nsstTargetName + " and version " + nsstVersion + " or ID already present in DB.";
			log.error(logErrorStr);
			throw new AlreadyExistingEntityException(logErrorStr);
		}

		List<NSST> nest_nssts=nsst.getNsstList();
		List<String> nest_nsstIds=new ArrayList<>();
		if(nest_nssts!=null && !nest_nssts.isEmpty()) {
			log.debug("Saving nested nssts");
			for (NSST nested_nsst : nest_nssts)
				nest_nsstIds.add(storeNssTemplate(nested_nsst));
		}else
			log.debug("No nested nssts are present");

		List<NSST> ref_nsst=new ArrayList<>();
		for(String id: nest_nsstIds){
			ref_nsst.add(nsstRepository.findByNsstId(id).get());
		}

		NSST target=new NSST(
				nsstTargetId,
				nsst.isOperationalState(),
				nsst.getAdministrativeState(),
				nsst.getNsInfo(),
				nsst.getSliceProfileList(),
				ref_nsst,
				nsst.getNsdInfo(),
				nsst.getType()
		);
		target.setNsstName(nsstTargetName);
		target.setNsstVersion(nsstVersion);

		nsstRepository.saveAndFlush(target);
		log.debug("Added NssTemplate with ID " + nsstTargetId);
		NssTemplateInfo nsstInfo = new NssTemplateInfo(UUID.randomUUID().toString(), nsstTargetId, nsstTargetName, nsstVersion, nsst.getNsstProvider(), target,null, null,false);
		nsstInfoRepository.saveAndFlush(nsstInfo);

		log.debug("Added NsTemplate Info with NsTemplate ID " + nsstTargetId);
		return nsstTargetId;
	}
}
