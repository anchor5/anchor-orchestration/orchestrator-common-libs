package it.nextworks.nfvmano.catalogues.template.repo;

import it.nextworks.nfvmano.catalogue.template.elements.NssTemplateInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NssTemplateInfoRepository extends JpaRepository<NssTemplateInfo, Long> {
    Optional<NssTemplateInfo> findByNssTemplateId(String id);
    Optional<NssTemplateInfo> findByNameAndNssTemplateVersion(String name, String version);
}
