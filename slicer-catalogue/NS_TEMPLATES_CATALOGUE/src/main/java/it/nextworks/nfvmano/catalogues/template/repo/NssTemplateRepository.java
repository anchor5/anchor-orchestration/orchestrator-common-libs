package it.nextworks.nfvmano.catalogues.template.repo;

import it.nextworks.nfvmano.libs.ifa.templates.nst.NSST;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NssTemplateRepository extends JpaRepository<NSST, Long> {
    Optional<NSST> findByNsstId(String id);
    Optional<NSST> findByNsstNameAndNsstVersion(String name, String version);
}
