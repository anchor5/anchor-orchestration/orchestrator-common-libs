package it.nextworks.nfvmano.catalogues.template.repo;

import it.nextworks.nfvmano.catalogue.template.elements.GsTemplateInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GsTemplateInfoRepository extends JpaRepository<GsTemplateInfo, Long> {
    Optional<GsTemplateInfo> findByGsTemplateId(String id);
    Optional<GsTemplateInfo> findByNameAndGsTemplateVersion(String name, String version);
}
