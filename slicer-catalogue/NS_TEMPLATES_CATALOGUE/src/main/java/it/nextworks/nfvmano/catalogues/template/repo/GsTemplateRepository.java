package it.nextworks.nfvmano.catalogues.template.repo;

import java.util.Optional;
import it.nextworks.nfvmano.libs.ifa.templates.gst.GST;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

@Repository
public interface GsTemplateRepository extends JpaRepository<GST, Long> {

    Optional<GST> findByGstId(String id);
    Optional<GST> findByGstNameAndGstVersion(String name, String version);
}
