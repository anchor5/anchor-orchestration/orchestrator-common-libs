/*
 * Copyright 2021 Nextworks s.r.l.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.nfvmano.catalogues.nstnesttranslation.algorithm;

import it.nextworks.nfvmano.catalogues.nstnesttranslation.NstNestTranslationAlgorithmType;
import it.nextworks.nfvmano.catalogues.nstnesttranslation.messages.TranslationRequest;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.templates.gst.*;
import it.nextworks.nfvmano.libs.ifa.templates.nst.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Boolean.FALSE;

public class NstNestStaticTranslationAlgorithm extends AbstractNstNestTranslationAlgorithm{

    private static final Logger log = LoggerFactory.getLogger(NstNestStaticTranslationAlgorithm.class);

    private String nestId;
    private GST nestTarget;
    private QItable qItable;

    public NstNestStaticTranslationAlgorithm(){
        super(NstNestTranslationAlgorithmType.STATIC);
        this.nestTarget=new GST();
        this.qItable=new QItable();
        this.qItable.setTable();
    }

    public GST translateNst(TranslationRequest request) throws FailedOperationException, MalformattedElementException {
        log.debug("NST translation into NEST with {} using Static translation algorithm", nestId);

        NST nst = request.getNst();
        nestTarget=request.getNest();
        this.nestId=request.getNestId();
        if(nestTarget==null) {
            nestTarget = new GST();
            nestTarget.setGstId(nestId);
        }

        //Setting base value
        if(nestTarget.getGstName()==null)
            nestTarget.setGstName(nst.getNstName());
        if(nestTarget.getGstVersion()==null)
            nestTarget.setGstVersion("v0.0.1");
        if(nestTarget.getGstProvider()==null)
            nestTarget.setGstProvider(nst.getNstProvider());

        //Translating value from Service profile list
        translatingProfileList(nst);

        /*NSST nsste2e= nst.getNsst();
        List<NSST> nssts=nsste2e.getNsstList();
        List<SliceProfile> sliceProfiles=null;

        for(NSST nsst:nssts)
            if(nsst.getType()== SliceSubnetType.RAN)
                sliceProfiles=nsst.getSliceProfileList();

        if(sliceProfiles==null)
            sliceProfiles=nsste2e.getSliceProfileList();

        translatingSliceProfiles(sliceProfiles)*/
        return nestTarget;
    }

    public void translatingProfileList(NST nst) throws FailedOperationException {
        log.debug("Translating Service Profile");
        List<NstServiceProfile> nsps=nst.getNstServiceProfileList();
        if(nsps.size()>1)
            throw new FailedOperationException("Multiple Service Profile are not supported yet");

        try {
            NstServiceProfile nsp=nsps.get(0);
            if(nestTarget.getMaxNUE()==null)
                nestTarget.setMaxNUE(new MaxNUE(nsp.getMaxNumberofUEs(), FALSE));
            //Setting Isolation Level into NEST
            if(nestTarget.getIsolation()==null)
                if(nsp.getResourceSharingLevel())
                    nestTarget.setIsolation(new Isolation(IsolationValue.Logical));
                else
                    nestTarget.setIsolation(new Isolation(IsolationValue.Physical));

            if(nestTarget.getAvailability()==0)
                nestTarget.setAvailability(nsp.getAvailability());
            if(nestTarget.getDelayTolerance()==null)
                nestTarget.setDelayTolerance(nsp.getDelayTolerance());

            if(nestTarget.getDeterministicCommunication()==null)
                nestTarget.setDeterministicCommunication(nsp.getDeterministicComm());

            if(nestTarget.getDownlinkThroughputNS()==null)
                nestTarget.setDownlinkThroughputNS(new DownlinkThroughputNS((int) nsp.getGuaThpt(), FALSE, nsp.getdLThptPerSlice()));
            if(nestTarget.getMaximumDownlinkThroughputUE()==0)
                nestTarget.setMaximumDownlinkThroughputUE(nsp.getdLThptPerUE());

            if(nestTarget.getUplinkThroughputNS()==null)
                nestTarget.setUplinkThroughputNS(new UplinkThroughputNS((int) nsp.getGuaThpt(), FALSE, nsp.getuLThptPerSlice()));
            if(nestTarget.getMaxSuppPacketSize()==0)
                nestTarget.setMaxSuppPacketSize(nsp.getMaxPktSize());
            if(nestTarget.getMaxNPDUsess()==null)
                nestTarget.setMaxNPDUsess(new MaxNPDUsess(nsp.getMaxNumberofConns()));

            if(nestTarget.getPerformanceMonitoring()==null)
                //Set as default value FrequencyValue.PerMinute
                nestTarget.setPerformanceMonitoring(new PerformanceMonitoring(nsp.getkPIMonitoringList(), FrequencyValue.PerMinute));
            if(nestTarget.getUserManagementOpeness()==null)
                nestTarget.setUserManagementOpeness(nsp.isUserMgmtOpen());
            if(nestTarget.getV2xCommunicationMode()==null)
                if(!nsp.isV2xMode())
                    nestTarget.setV2xCommunicationMode(V2xCommValue.NotSupported);
                else
                    nestTarget.setV2xCommunicationMode(V2xCommValue.YES_NR);
            if(nestTarget.getSupportedDeviceVelocity()==0)
                nestTarget.setSupportedDeviceVelocity(nsp.getuESpeed());
            if(nestTarget.getUeDensity()==0)
                nestTarget.setUeDensity(nsp.getTermDensity());

            //Setting QoS parameters for nest, it depends on slice type and latency
            setRightQos(nsp);
        } catch (MalformattedElementException e){
            log.error("Malformatted Determinitstic Communication or Performance Monitoring attribute attribute");
            e.printStackTrace();
        }
        log.debug("Service Profile correctly translated");
    }

    public void setRightQos(NstServiceProfile nsp) throws MalformattedElementException{
        log.debug("Translating QoS list");
        List<SliceQoSparams> qosList;
        if(nestTarget.getSliceQoSparams()==null)
            qosList=new ArrayList<>();
        else
            qosList=nestTarget.getSliceQoSparams();
        int pktDelay=nsp.getLatency();
        switch (nsp.getResourceType()) {
            case DelayCriticalGBR:
                if(pktDelay<=5 && !qosList.contains(qItable.defaultValue.get(QosValue.qi86)))
                    qosList.add(qItable.defaultValue.get(QosValue.qi86));
                else if(pktDelay<=10 && !qosList.contains(qItable.defaultValue.get(QosValue.qi82)))
                    qosList.add(qItable.defaultValue.get(QosValue.qi82));
                else if(!qosList.contains(qItable.defaultValue.get(QosValue.qi84)))
                    qosList.add(qItable.defaultValue.get(QosValue.qi84));
                break;
            case GBR:
                if(pktDelay<=50 && !qosList.contains(qItable.defaultValue.get(QosValue.qi3)))
                    qosList.add(qItable.defaultValue.get(QosValue.qi3));
                else if(pktDelay<=75 && !qosList.contains(qItable.defaultValue.get(QosValue.qi65)))
                    qosList.add(qItable.defaultValue.get(QosValue.qi65));
                else if(pktDelay<=100 && !qosList.contains(qItable.defaultValue.get(QosValue.qi1)))
                    qosList.add(qItable.defaultValue.get(QosValue.qi1));
                else if(pktDelay<=150 && !qosList.contains(qItable.defaultValue.get(QosValue.qi2)))
                    qosList.add(qItable.defaultValue.get(QosValue.qi2));
                else if(pktDelay<=300 && !qosList.contains(qItable.defaultValue.get(QosValue.qi72)))
                    qosList.add(qItable.defaultValue.get(QosValue.qi72));
                else if(!qosList.contains(qItable.defaultValue.get(QosValue.qi76)))
                    qosList.add(qItable.defaultValue.get(QosValue.qi76));
                break;
            case NonGBR:
                if(pktDelay<=10 && !qosList.contains(qItable.defaultValue.get(QosValue.qi80)))
                    qosList.add(qItable.defaultValue.get(QosValue.qi80));
                else if(pktDelay<=50 && !qosList.contains(qItable.defaultValue.get(QosValue.qi79)))
                    qosList.add(qItable.defaultValue.get(QosValue.qi79));
                else if(pktDelay<=60 && !qosList.contains(qItable.defaultValue.get(QosValue.qi69)))
                    qosList.add(qItable.defaultValue.get(QosValue.qi69));
                else if(pktDelay<=100 && !qosList.contains(qItable.defaultValue.get(QosValue.qi7)))
                    qosList.add(qItable.defaultValue.get(QosValue.qi7));
                else if(pktDelay<=200 && !qosList.contains(qItable.defaultValue.get(QosValue.qi70)))
                    qosList.add(qItable.defaultValue.get(QosValue.qi70));
                else if(!qosList.contains(qItable.defaultValue.get(QosValue.qi9)))
                    qosList.add(qItable.defaultValue.get(QosValue.qi9));
                break;
            default:
                break;
        }
        nestTarget.setSliceQoSparams(qosList);
    }

    //Slice profile attributes are already mapped into nest attributes previously using service profile attributes
    public void translatingSliceProfiles(List<SliceProfile> sliceProfiles) throws FailedOperationException {
        log.debug("Translating slice profile");
        if(sliceProfiles.size()>1)
            throw new FailedOperationException("Multiple slice profile not supported");

        SliceProfile sp=sliceProfiles.get(0);
        List<EMBBPerfReq> embbPerfReqs=sp.geteMBBPerfReq();
        List<URLLCPerfReq> urllcPerfReqs=sp.getuRLLCPerfReq();
        if(embbPerfReqs!=null){
            if(embbPerfReqs.size()>1)
                throw new FailedOperationException("Multiple EMBB performance requirement not supported");


        }else{
            if(urllcPerfReqs.size()>1)
                throw new FailedOperationException("Multiple URLLC performance requirement not supported");


        }
    }
}
