/*
 * Copyright 2021 Nextworks s.r.l.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.nfvmano.catalogues.nstnesttranslation.service;

import it.nextworks.nfvmano.catalogue.template.interfaces.NestCatalogueInterface;
import it.nextworks.nfvmano.catalogue.template.interfaces.NsTemplateCatalogueInterface;
import it.nextworks.nfvmano.catalogue.template.messages.nest.OnBoardNesTemplateRequest;
import it.nextworks.nfvmano.catalogues.nstnesttranslation.NstNestTranslationInterface;
import it.nextworks.nfvmano.catalogues.nstnesttranslation.algorithm.AbstractNstNestTranslationAlgorithm;
import it.nextworks.nfvmano.catalogues.nstnesttranslation.algorithm.NstNestStaticTranslationAlgorithm;
import it.nextworks.nfvmano.catalogues.nstnesttranslation.messages.TranslationRequest;
import it.nextworks.nfvmano.libs.ifa.common.elements.Filter;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.common.messages.GeneralizedQueryRequest;
import it.nextworks.nfvmano.libs.ifa.templates.gst.GST;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Service
public class NstNestTranslationService implements NstNestTranslationInterface {
    private static final Logger log = LoggerFactory.getLogger(NstNestTranslationService.class);

    @Value("${algorithm.type}")
    private String algorithmType;

    @Autowired
    private NestCatalogueInterface nestCatalogueService;

    @Autowired
    private NsTemplateCatalogueInterface nsTemplateCatalogueService;

    private AbstractNstNestTranslationAlgorithm algorithm;

    public NstNestTranslationService(){}

    @PostConstruct
    public void initTranslationService() {
        log.debug("Initializing translator");
        if (algorithmType.equals("STATIC")) {
            log.debug("The slicer is configured to operate with a basic translator.");
            algorithm = new NstNestStaticTranslationAlgorithm();
        } else {
            log.error("Algorithm not configured!");
        }
    }

    @Override
    public GST translateNst(TranslationRequest request) throws FailedOperationException, MalformattedElementException {
        request.isValid();
        if(request.getNstId()!=null){
            if(nsTemplateCatalogueService==null)
                throw new FailedOperationException("NsTemplateCatalogueService not present, it is not possible retrieve NST from DB");

            Map<String, String> filterParams=new HashMap<>();
            filterParams.put("NST_ID", request.getNstId());
            GeneralizedQueryRequest query=new GeneralizedQueryRequest(new Filter(filterParams),null);
            try {
                request.setNst(nsTemplateCatalogueService.queryNsTemplate(query).getNsTemplateInfos().get(0).getNST());
            } catch (Exception e){
                log.error(e.getMessage());
            }
        }
        GST gst=algorithm.translateNst(request);
        log.debug("Nest correctly translated");
        if(nestCatalogueService!=null) {
            log.debug("Nest onboaring active");
            OnBoardNesTemplateRequest req = new OnBoardNesTemplateRequest(gst, request.getNstId(), null, null);
            try {
                nestCatalogueService.onBoardNesTemplate(req);
            } catch (Exception e) {
                log.error("Impossible onboarding of translated nest");
            }
        }
        return gst;
    }

    public void setNestCatalogueService(NestCatalogueInterface nestCatalogueService){
        this.nestCatalogueService=nestCatalogueService;
    }

    public void setNsTemplateCatalogueService(NsTemplateCatalogueInterface nsTemplateCatalogueService){
        this.nsTemplateCatalogueService=nsTemplateCatalogueService;
    }
}
