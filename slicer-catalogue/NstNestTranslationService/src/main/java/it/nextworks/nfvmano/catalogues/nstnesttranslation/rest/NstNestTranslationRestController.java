/*
 * Copyright 2021 Nextworks s.r.l.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.nfvmano.catalogues.nstnesttranslation.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.nextworks.nfvmano.catalogues.nstnesttranslation.messages.TranslationRequest;
import it.nextworks.nfvmano.catalogues.nstnesttranslation.service.NstNestTranslationService;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.templates.gst.GST;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

//@Api(tags = "Network Slice Template translation API")
@RestController
@CrossOrigin
@RequestMapping("/ns/catalogue")
public class NstNestTranslationRestController {
    private static final Logger log = LoggerFactory.getLogger(NstNestTranslationRestController.class);

    @Autowired
    private NstNestTranslationService nstNestTranslationService;

    @Value("${sebastian.admin}") //FIXME Check about the NSMF Admin tenant
    private String adminTenant;

    public NstNestTranslationRestController(){}

    /*private static String getUserFromAuth(Authentication auth) {
        Object principal = auth.getPrincipal();
        if (!UserDetails.class.isAssignableFrom(principal.getClass())) {
            throw new IllegalArgumentException("Auth.getPrincipal() does not implement UserDetails");
        }
        return ((UserDetails) principal).getUsername();
    }*/

    @RequestMapping(value = "/nsttranslator", method = RequestMethod.POST)
    @ApiOperation(value = "Translate an NST into NEST")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "The GST generated from the translation request", response = String.class),
            //@ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
            //@ApiResponse(code = 409, message = "There is a conflict with the request", response = ResponseEntity.class),
            //@ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)

    })
    public ResponseEntity<?> translateNst(@RequestBody TranslationRequest request, Authentication auth){
        log.debug("Receive request to translate NST");
        /*String user = getUserFromAuth(auth);
        if (!user.equals(adminTenant)) {
            log.warn("Request refused as tenant {} is not admin.", user);
            return new ResponseEntity<>("Unauthorized", HttpStatus.UNAUTHORIZED);
        }*/

        try {
            GST gst = nstNestTranslationService.translateNst(request);
            return new ResponseEntity<GST>(gst, HttpStatus.OK);
        } catch (FailedOperationException e){
            log.error("Impossible to translate NST");
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (MalformattedElementException e){
            log.error("Malformatted request");
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

}
