/*
 * Copyright 2018 Nextworks s.r.l.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.nfvmano.catalogue.blueprint.messages;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.nextworks.nfvmano.catalogue.template.elements.NstConfigurationRule;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.OnBoardVnfPackageRequest;
import it.nextworks.nfvmano.libs.ifa.common.InterfaceMessage;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.descriptors.nsd.Nsd;
import it.nextworks.nfvmano.catalogue.blueprint.elements.VsdNsdTranslationRule;
import it.nextworks.nfvmano.libs.ifa.descriptors.nsd.Pnfd;
import it.nextworks.nfvmano.libs.ifa.templates.nst.NST;
import it.nextworks.nfvmano.libs.ifa.templates.gst.GST;

public class OnBoardBlueprintRequest implements InterfaceMessage {

	private List<Nsd> nsds = new ArrayList<>();
	private List<String> nsdIds=new ArrayList<>();
	private List<VsdNsdTranslationRule> translationRules = new ArrayList<>();
	private List<NST> nsts = new ArrayList<>();
	private List<GST> gsts = new ArrayList<>();
	private List<Pnfd> pnfds = new ArrayList<>();
	private List<NstConfigurationRule> configurationRules = new ArrayList<>();

	private String owner;

	public OnBoardBlueprintRequest() { }

	/**
	 * Constructor
	 *
	 * @param nsds
	 * @param nsdIds
	 * @param translationRules
	 *
	 */
	public OnBoardBlueprintRequest(List<Nsd> nsds,
								   List<String> nsdIds,
								   List<VsdNsdTranslationRule> translationRules,
								   List<NST> nsts,
								   List<GST> gsts,
								   List<Pnfd> pnfds,
								   List<NstConfigurationRule> configurationRules) {
		if (nsds != null) this.nsds = nsds;
		if (nsdIds!=null) this.nsdIds=nsdIds;
		if (translationRules != null) this.translationRules = translationRules;
		if (nsts!=null) this.nsts = nsts;
		if (gsts!=null) this.gsts=gsts;
		if (pnfds!=null) this.pnfds = pnfds;
		if (configurationRules != null) this.configurationRules = configurationRules;
	}

	public void setGsts(List<GST> gsts) {
		this.gsts = gsts;
	}

	public List<GST> getGsts() {
		return gsts;
	}

	public void setNsds(List<Nsd> nsds) {
		this.nsds = nsds;
	}

	public void setTranslationRules(List<VsdNsdTranslationRule> translationRules) {
		this.translationRules = translationRules;
	}

	public void setNsts(List<NST> nsts) {
		this.nsts = nsts;
	}

	public void setPnfds(List<Pnfd> pnfds) {
		this.pnfds = pnfds;
	}

	public void setConfigurationRules(List<NstConfigurationRule> configurationRules) {
		this.configurationRules = configurationRules;
	}

	public void setNsdIds(List<String> nsdIds) {
		this.nsdIds = nsdIds;
	}

	public List<String> getNsdIds() {
		return nsdIds;
	}

	/**
	 * @return the nsds
	 */
	public List<Nsd> getNsds() {
		return nsds;
	}

	/**
	 * @return the pnfds
	 */
	public List<Pnfd> getPnfds(){ return pnfds; }

	/**
	 * @return the translationRules
	 */
	public List<VsdNsdTranslationRule> getTranslationRules() {
		return translationRules;
	}

	public List<NST> getNsts() {
		return nsts;
	}

	@JsonIgnore
	public void setBlueprintIdInTranslationRules(String blueprintId) {
		for (VsdNsdTranslationRule tr : translationRules)
			tr.setBlueprintId(blueprintId);
	}

	@JsonIgnore
	public void setNsdInfoIdInTranslationRules(String nsdInfoId, String nsdId, String nsdVersion) {
		for (VsdNsdTranslationRule tr : translationRules) {
			if (tr.matchesNsdIdAndNsdVersion(nsdId, nsdVersion)) tr.setNsdInfoId(nsdInfoId);
		}
	}



	@Override
	public void isValid() throws MalformattedElementException {
		//if (nsds.isEmpty()) throw new MalformattedElementException("Onboard VS blueprint request without NSD");
		//if (translationRules.isEmpty()) throw new MalformattedElementException("Onboard VS blueprint request without translation rules");
		if(!translationRules.isEmpty()) for (VsdNsdTranslationRule tr : translationRules) tr.isValid();
		if(gsts!=null && nsts!=null)
			throw new MalformattedElementException("Only list of GST or list of NST are permitted");
		if(gsts!=null && nsds==null && nsdIds==null)
			throw new MalformattedElementException("If GST list is present, a list of NSD or a list of NSD ID is required");
		if(!configurationRules.isEmpty()) for (NstConfigurationRule cr : configurationRules) cr.isValid();
		//for (Nsd nsd : nsds) nsd.isValid();
	}

	@JsonIgnore
	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public List<NstConfigurationRule> getConfigurationRules() {
		return configurationRules;
	}
}
