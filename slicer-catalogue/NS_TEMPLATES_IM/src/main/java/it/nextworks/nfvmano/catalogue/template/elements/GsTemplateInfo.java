package it.nextworks.nfvmano.catalogue.template.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.nextworks.nfvmano.libs.ifa.common.InterfaceInformationElement;
import it.nextworks.nfvmano.libs.ifa.common.enums.OperationalState;
import it.nextworks.nfvmano.libs.ifa.common.enums.UsageState;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.templates.gst.GST;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
public class GsTemplateInfo implements InterfaceInformationElement {
    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

    private String gstInfoId;
    private String gsTemplateId;
    private String name;
    private String gsTemplateVersion;
    private String designer;

    @Transient
    private GST gst;

    private String referenceNstId;

    @ElementCollection(targetClass=String.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<String> referenceNsstsId;

    private OperationalState operationalState;
    private UsageState usageState;
    private boolean deletionPending;

    public GsTemplateInfo(){}

    public GsTemplateInfo(String gstInfoId, String gsTemplateId,
                          String name, String gsTemplateVersion,
                          String designer, GST gst,
                          OperationalState operationalState, UsageState usageState,
                          boolean deletionPending){
        this.gstInfoId=gstInfoId;
        this.gsTemplateId=gsTemplateId;
        this.name=name;
        this.gsTemplateVersion=gsTemplateVersion;
        this.designer=designer;
        this.gst=gst;
        this.operationalState=operationalState;
        this.usageState=usageState;
        this.deletionPending=deletionPending;
    }

    @Override
    public void isValid() throws MalformattedElementException {
        if (gstInfoId == null) throw new MalformattedElementException("GsTemplate info without ID");
        if (gsTemplateVersion == null) throw new MalformattedElementException("GsTemplate info without version");
        if (name == null) throw new MalformattedElementException("GsTemplate info without name");
        if (gst==null)
            throw new MalformattedElementException("GST cannot be null");
    }

    public String getGstInfoId() {
        return gstInfoId;
    }

    public void setGstInfoId(String gstInfoId) {
        this.gstInfoId = gstInfoId;
    }

    public String getGsTemplateId() {
        return gsTemplateId;
    }

    public void setGsTemplateId(String gsTemplateId) {
        this.gsTemplateId = gsTemplateId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGsTemplateVersion() {
        return gsTemplateVersion;
    }

    public void setGsTemplateVersion(String gsTemplateVersion) {
        this.gsTemplateVersion = gsTemplateVersion;
    }

    public String getDesigner() {
        return designer;
    }

    public void setDesigner(String designer) {
        this.designer = designer;
    }

    public GST getGst() {
        return gst;
    }

    public void setGst(GST gst) {
        this.gst = gst;
    }

    public String getReferenceNstId() {
        return referenceNstId;
    }

    public void setReferenceNstId(String referenceNstId) {
        this.referenceNstId = referenceNstId;
    }

    public List<String> getReferenceNsstsId() {
        return referenceNsstsId;
    }

    public void setReferenceNsstsId(List<String> referenceNsstsId) {
        this.referenceNsstsId = referenceNsstsId;
    }

    public OperationalState getOperationalState() {
        return operationalState;
    }

    public void setOperationalState(OperationalState operationalState) {
        this.operationalState = operationalState;
    }

    public UsageState getUsageState() {
        return usageState;
    }

    public void setUsageState(UsageState usageState) {
        this.usageState = usageState;
    }

    public boolean isDeletionPending() {
        return deletionPending;
    }

    public void setDeletionPending(boolean deletionPending) {
        this.deletionPending = deletionPending;
    }
}
