package it.nextworks.nfvmano.catalogue.template.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.nextworks.nfvmano.libs.ifa.common.InterfaceInformationElement;
import it.nextworks.nfvmano.libs.ifa.common.enums.OperationalState;
import it.nextworks.nfvmano.libs.ifa.common.enums.UsageState;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.templates.gst.GST;
import it.nextworks.nfvmano.libs.ifa.templates.nst.NSST;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
public class NssTemplateInfo implements InterfaceInformationElement {
    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

    private String nsstInfoId;
    private String nssTemplateId;
    private String name;
    private String nssTemplateVersion;
    private String designer;

    @Transient
    private NSST nsst;

    private String referenceGstId;
    private String referenceNstId;

    private OperationalState operationalState;
    private UsageState usageState;
    private boolean deletionPending;

    public NssTemplateInfo(){}

    public NssTemplateInfo(String nsstInfoId, String nssTemplateId,
                           String name, String nssTemplateVersion,
                           String designer, NSST nsst,
                           OperationalState operationalState, UsageState usageState,
                           boolean deletionPending){
        this.nsstInfoId=nsstInfoId;
        this.nssTemplateId=nssTemplateId;
        this.name=name;
        this.nssTemplateVersion=nssTemplateVersion;
        this.designer=designer;
        this.nsst=nsst;
        this.operationalState=operationalState;
        this.usageState=usageState;
        this.deletionPending=deletionPending;
    }

    @Override
    public void isValid() throws MalformattedElementException {
        if (nsstInfoId == null) throw new MalformattedElementException("NssTemplate info without ID");
        if (nssTemplateVersion == null) throw new MalformattedElementException("NssTemplate info without version");
        if (name == null) throw new MalformattedElementException("NssTemplate info without name");
        if (nsst==null)
            throw new MalformattedElementException("NSST cannot be null");
    }

    public String getNsstInfoId() {
        return nsstInfoId;
    }

    public void setNsstInfoId(String nsstInfoId) {
        this.nsstInfoId = nsstInfoId;
    }

    public String getNssTemplateId() {
        return nssTemplateId;
    }

    public void setNssTemplateId(String nssTemplateId) {
        this.nssTemplateId = nssTemplateId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNssTemplateVersion() {
        return nssTemplateVersion;
    }

    public void setNssTemplateVersion(String nssTemplateVersion) {
        this.nssTemplateVersion = nssTemplateVersion;
    }

    public String getDesigner() {
        return designer;
    }

    public void setDesigner(String designer) {
        this.designer = designer;
    }

    public NSST getNsst() {
        return nsst;
    }

    public void setNsst(NSST nsst) {
        this.nsst = nsst;
    }

    public String getReferenceGstId() {
        return referenceGstId;
    }

    public void setReferenceGstId(String referenceGstId) {
        this.referenceGstId = referenceGstId;
    }

    public String getReferenceNstId() {
        return referenceNstId;
    }

    public void setReferenceNstId(String referenceNstId) {
        this.referenceNstId = referenceNstId;
    }

    public OperationalState getOperationalState() {
        return operationalState;
    }

    public void setOperationalState(OperationalState operationalState) {
        this.operationalState = operationalState;
    }

    public UsageState getUsageState() {
        return usageState;
    }

    public void setUsageState(UsageState usageState) {
        this.usageState = usageState;
    }

    public boolean isDeletionPending() {
        return deletionPending;
    }

    public void setDeletionPending(boolean deletionPending) {
        this.deletionPending = deletionPending;
    }
}
