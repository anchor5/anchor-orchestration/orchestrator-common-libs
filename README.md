# Vertical Slicer Common Libs

Common library set for the Vertical Slicer

## Installation
- Create a folder in you workspace, this will be your root folder for the project. (ie ANCHOR/) 
- Checkout in the root folder this repository
- make sure you have installed java 8 and maven 3.X 
 *(tested with openjdk version 1.8.0_312 and Apache Maven 3.6.3)*
- in the root folder run this command:

> mvn clean install -f orchestrator-common-libs/pom.xml

- This will install all the dependency in your system

