/*
 * Copyright (c) 2021 Nextworks s.r.l
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.nfvmano.libs.ifa.templates.gst;

public enum QosValue {
    qi1, qi2, qi3, qi4, qi65, qi66, qi67, qi75,qi71, qi72, qi73, qi74, qi76, qi5, qi6, qi7, qi8, qi9, qi69, qi70, qi79, qi80, qi82, qi83, qi84, qi85, qi86
}