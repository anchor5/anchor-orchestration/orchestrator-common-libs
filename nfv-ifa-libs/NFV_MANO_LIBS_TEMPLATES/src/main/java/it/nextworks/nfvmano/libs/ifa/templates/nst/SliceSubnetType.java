package it.nextworks.nfvmano.libs.ifa.templates.nst;

public enum SliceSubnetType {
    RAN,
    CORE,
    TRANSPORT,
    VAPP,
    E2E
}
