package it.nextworks.nfvmano.libs.ifa.templates.common;

public enum ResTypeValue {
        GBR,
        DelayCriticalGBR,
        NonGBR
}
