package it.nextworks.nfvmano.libs.ifa.templates.gst;

public enum NROperatingBand {
    n1, n2, n3, n5, n7, n8, n12, n20, n25, n28, n34, n38, n39, n40, n41, n51, n66, n70, n71, n75, n76, n77, n78, n79, n80,
    n81, n82, n83, n84, n86
}
