package it.nextworks.nfvmano.libs.ifa.templates.gst;

public enum EUTRAOperatingBand {
    eutra1, eutra2, eutra3, eutra4, eutra5, eutra7, eutra8, eutra9, eutra10, eutra11, eutra12, eutra13, eutra14, eutra17, eutra18, eutra19, eutra20, eutra21, eutra22, eutra24,
    eutra25, eutra26, eutra27, eutra28, eutra29, eutra31, eutra32, eutra33, eutra34, eutra35, eutra36, eutra37, eutra38, eutra39, eutra40, eutra41, eutra42, eutra43, eutra44,
    eutra45, eutra46, eutra47, eutra48, eutra49, eutra50, eutra51, eutra52, eutra65, eutra66, eutra67, eutra68, eutra69, eutra70, eutra71, eutra72, eutra73, eutra74, eutra75,
    eutra76, eutra85
}
