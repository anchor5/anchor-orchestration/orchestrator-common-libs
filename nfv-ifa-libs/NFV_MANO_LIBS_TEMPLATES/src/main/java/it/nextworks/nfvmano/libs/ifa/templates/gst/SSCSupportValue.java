package it.nextworks.nfvmano.libs.ifa.templates.gst;

public enum SSCSupportValue {
    SSCmode1,
    SSCmode2,
    SSCmode3
}
