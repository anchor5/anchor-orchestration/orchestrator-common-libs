package it.nextworks.nfvmano.libs.ifa.templates.gst;

public enum RCIValue {
    NotSupported,
    PassiveInvestigation,
    ActiveInvestigation
}
